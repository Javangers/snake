package snake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

@SuppressWarnings("serial")

public class Grafik extends JPanel {

	public Snake snake;
	//Zeichen Elemente
	public Grafik(Snake snake){
		this.snake = snake;
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 805, 700);
		Font schriftart = new Font( "SansSerif", Font.PLAIN, 20 );
		g.setColor(Color.GREEN);
		for(Point punkt : snake.sTeile){
			g.fillRect(punkt.x * Snake.GROESSE, punkt.y * Snake.GROESSE, 
					Snake.GROESSE, Snake.GROESSE);
		}
		//Farbe der Schlange
		g.fillRect(snake.kopf.x * Snake.GROESSE, snake.kopf.y * Snake.GROESSE, 
				Snake.GROESSE, Snake.GROESSE);
		
		//Farbe der Kirsche
		g.setColor(Color.RED);
		g.fillRect(snake.kirsche.x * Snake.GROESSE, snake.kirsche.y * Snake.GROESSE, 
				Snake.GROESSE, Snake.GROESSE);
		
		//Scoreboard anzeige
		String string = "Score: " + snake.punkte + ", Laenge: " + snake.sLaenge;
		g.setColor(Color.white);
		g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), 10);
		
		//Verloren Anzeige
		string = "Verloren!";
		String stringmenue = "Druecke Leertaste um neu zu Starten.";
		if (snake.ende){
			g.setFont(schriftart);
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 5f), (int) snake.dim.getHeight() / 4);
			g.setFont(schriftart);
			g.drawString(stringmenue, (int) (getWidth() / 2 - string.length() * 18f), (int) snake.dim.getHeight() /3);
			

		}
}
	
}
