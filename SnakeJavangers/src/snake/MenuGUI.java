package snake;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;

public class MenuGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	
	/**
	 * Erstellen des Fensters mit absolut Layout.
	 */
	
	
	public MenuGUI() {
		setTitle("Snake Men�");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 190);
		contentPane = new JPanel();
		contentPane.setBackground(Color.YELLOW);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setVisible(true);
		setResizable(false);
		contentPane.setLayout(null);
		
		
		
		//Label:
		JLabel lblUser = new JLabel("Spielername:");
		lblUser.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 16));
		lblUser.setHorizontalAlignment(SwingConstants.LEFT);
		lblUser.setBackground(Color.BLACK);
		lblUser.setBounds(68, 10, 115, 29);
		contentPane.add(lblUser);
	
		
		//Textfeld zum eintragen:
		JTextField txtUser = new JTextField("\"H. Fischer\"");
		txtUser.setFont(new Font("Calibri", Font.ITALIC, 16));
		txtUser.setBounds(158, 10, 115, 29);
		contentPane.add(txtUser);
		
		
		//Startbutton:
		JButton btnStart = new JButton("Start");
		btnStart.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Snake();
				dispose();
			}
		});
		btnStart.setBounds(158, 43, 115, 29);
		contentPane.add(btnStart);
		
		
		//Optionsbutton:
		JButton btnOptionen = new JButton("Optionen");
		btnOptionen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Optionsmenue();
				dispose();
			}
		});
		
		btnOptionen.setBounds(158, 78, 115, 29);
		contentPane.add(btnOptionen);
		
		
		//Highscorebutton ohne Funktion -> Highscore fehlt
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//new HighscoreMuene();
			}
		});
		
		btnHighscore.setBounds(158, 112, 115, 29);
		contentPane.add(btnHighscore);
		
	}
}
