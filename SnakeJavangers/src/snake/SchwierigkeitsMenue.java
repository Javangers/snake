package snake;


import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;

	

public class  SchwierigkeitsMenue extends JFrame implements ActionListener 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static SchwierigkeitsMenue schwierigkeitsmenue;
	private JButton btnEinfach;
	private JButton btnNormal;
	private JButton btnSchwer;
	
	
	public SchwierigkeitsMenue()
	{
	setTitle("Option");
	setSize(805,700);
	setResizable(false);
	setLayout(null);
	setVisible(true);	 
	
	btnEinfach = new JButton("Einfach");
	btnEinfach.setBounds(322,143,160,40);
	add(btnEinfach); 
	
	btnNormal = new JButton("Normal");
	btnNormal.setBounds(322,336,160,40);
	add(btnNormal);	
	
	btnSchwer = new JButton("Schwer");
	btnSchwer.setBounds(322,529,160,40);
	add(btnSchwer);
	
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	
		
	 	btnEinfach.addActionListener (new ActionListener()     			
		 	{
		 		
				public void actionPerformed(ActionEvent e)		// Einfach Button 
				 {
					Snake.setModus(3);	
					new Snake();
					dispose();
				}	
			}); 
	 	
	 	btnNormal.addActionListener (new ActionListener()     			
	 	{
	 		
			public void actionPerformed(ActionEvent e)		// Einfach Button 
			 {
				Snake.setModus(2);	
				new Snake();
				dispose();
			}	
		}); 
	 	
	 	btnSchwer.addActionListener (new ActionListener()     			
	 	{
	 		
			public void actionPerformed(ActionEvent e)		// Einfach Button 
			 {
				Snake.setModus(1);	
				new Snake();
				dispose();
			}	
		}); 
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
		
}
/// + **    Ende */