package snake;


// Enth�lt Methoden zum zeichnen geometrischer Figuren
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;


public class Snake extends JFrame implements ActionListener, KeyListener { 
	
/**
	 * 
	 */
private static final long serialVersionUID = 1L;

public static Snake snake;
public Grafik grafik;
//public MenueGUI menueGUI;
public PlanB planB;
public NervigeMusik nrevigemusik;
public Dimension dim;
public Boolean ende = false;
public ArrayList<Point> sTeile = new ArrayList<Point>();
public Timer timer = new Timer(20, this);
public static final int HOCH = 0, RUNTER = 1, LINKS = 2, RECHTS = 3, GROESSE = 10;
public static int ticks; 
public static int schnelligkeit = 2;

public int richtung = RUNTER;

public int punkte;

public int sLaenge = 10; 
public Point kopf, kirsche;
public Random random;


	public Snake(){
		 // Erzeugung eines neuen Frames mit dem Titel "Snake"        
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		setTitle("Snake");
		setSize(805,700);
		setLocation(dim.width / 2 - getWidth() / 2, dim.height / 2 - getHeight() / 2);
		setResizable(false);
		add(grafik = new Grafik(this));
		//add(menueGUI = new MenueGUI());
		setVisible(true); 
		addKeyListener(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		starteSpiel();
		  
	}
	public void starteSpiel()
	{
		ende = false;
		punkte = 0;
		sLaenge = 5;
		ticks = 0;
		richtung = RUNTER;
		kopf = new Point(0, -1);
		random = new Random();
		sTeile.clear();
		kirsche = new Point(random.nextInt(79), random.nextInt(66));
		timer.start();
	}
	
		@Override
		//Aktualiesiert die Grafiken
		public void actionPerformed(ActionEvent arg0) {
			grafik.repaint();
			ticks ++;
			
			//Snake Geschwindigkeit und Richtung
			if (ticks % schnelligkeit == 0 && kopf != null && !ende){
				
				sTeile.add(new Point(kopf.x, kopf.y));
				
				if (richtung == HOCH){	
					if(kopf.y -1 >= 0 && keineBeruehrung(kopf.x, kopf.y - 1)){
						kopf = new Point(kopf.x, kopf.y -1);
					}
					else{
						ende = true;
					}
				}
				
				if (richtung == RUNTER){
					if(kopf.y +1 < 67 && keineBeruehrung(kopf.x, kopf.y +1)){
						kopf =new Point(kopf.x, kopf.y +1);
					}
					else{
						ende = true;
					}
				}
				
				if (richtung == LINKS){
					if(kopf.x -1 >= 0 && keineBeruehrung(kopf.x - 1, kopf.y)){
						kopf =new Point(kopf.x -1, kopf.y);
					}
					else{
						ende = true;
					}
				}
				
				if (richtung == RECHTS){
					if(kopf.x +1 < 80 && keineBeruehrung(kopf.x + 1, kopf.y)){
						kopf = new Point(kopf.x +1, kopf.y);
					}
					else{
						ende = true;
					}
				}
				//Entfernt den letzten Teil der Schlange
				if(sTeile.size() > sLaenge){
					sTeile.remove(0);
				}
				
				//Zuf�lliges erzeugen der Kirschen
				if (kirsche != null){
					if(kopf.x == kirsche.x && kopf.y == kirsche.y){
						punkte += 10;
						sLaenge+=5;
						kirsche.setLocation(random.nextInt(79), random.nextInt(66));
					}
				}
		}

				
}
		public static int getTicks() {
			return ticks;
		}
		public static void setModus(int modus) {
			schnelligkeit = modus;
		}
		//Stellt fest ob der Kopf sich selbst ber�hrt
		public boolean keineBeruehrung(int x, int y)
		{
			for (Point point : sTeile)
			{
				if (point.equals(new Point(x, y)))
				{
					return false;
				}
			}
			return true;
		}
		


		//Steuerung der Schlange mit den Pfeiltasten
		@Override
		public void keyPressed(KeyEvent e) {
			int i = e.getKeyCode();

			if ((i == KeyEvent.VK_LEFT)&& richtung != RECHTS)
			{
				richtung = LINKS;
			}
			
			if ((i == KeyEvent.VK_RIGHT) && richtung != LINKS)
			{
				richtung = RECHTS;
			}

			if ((i == KeyEvent.VK_UP) && richtung != RUNTER)
			{
				richtung = HOCH;
			}

			if ((i == KeyEvent.VK_DOWN) && richtung != HOCH)
			{
				richtung = RUNTER;
			}
			
			if (i == KeyEvent.VK_SPACE)
			{
				if (ende){
					starteSpiel();
				}
			}
		}
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
}